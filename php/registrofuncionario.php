<?php
error_reporting(0);
date_default_timezone_set("America/Bogota");
extract($_REQUEST);
extract($_POST);
extract($_FILES);
require_once('../config/conexion.php');

$conexion = new Conexion();

if ($VALIDACCESO == "reg_tipouser") {
    $consulta = "SELECT ID,NOMBRE FROM EVADB.RED_TIPO_USUARIO";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
       // $numresult = $stmt->fetchColumn();
       // if ($numresult > 0) {
            $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);
       // }
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_parametro") {
    $consulta = "SELECT * FROM RED_PARAMETROS ORDER BY ORDEN ASC ";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = $stmt->fetchColumn();
        if ($numresult > 0) {
            $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);
        }
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_tipoidentificacion") {
    $consulta = "SELECT ID, ALIAS, NOMBRE, ESTADO FROM EVADB.BASE_TIPOIDENTIFICACION WHERE ESTADO=1";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
      //  $numresult = $stmt->fetchColumn();
        //if ($numresult > 0) {
            $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);
        //}
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if($VALIDACCESO == "reg_pais"){
     $consulta = "SELECT ID,NOMBRE,ESTADO FROM BASE_PAIS ORDER BY NOMBRE ASC ";
    try {
         $db = $conexion->ConectarBD();
         $stmt = $db->prepare($consulta);
         $stmt->execute();
         $numresult = $stmt->fetchColumn();
                if($numresult>0){
                    $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
                    $db = null;
                    $stmt = null;
                   header('Content-Type: application/json');
                   $datos = array( 'code' => 100,'data' => $RecSet);
                   echo json_encode($datos, JSON_FORCE_OBJECT);
               } 
        
            }  catch (PDOException $e){
               echo ($e->getMessage());
         } 
    }

if ($VALIDACCESO == "reg_dpto") {
    $consulta = "SELECT ID,PAIS_ID,NOMBRE,ESTADO FROM BASE_DEPARTAMENTO WHERE PAIS_ID=$id ORDER BY NOMBRE ASC";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = $stmt->fetchColumn();
        if ($numresult > 0) {
            $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);
        }
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_cuidad") {
    $consulta = "SELECT ID, DEPARTAMENTO_ID, NOMBRE, ESTADO FROM BASE_MUNICIPIO WHERE DEPARTAMENTO_ID=$id ORDER BY NOMBRE ASC";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = $stmt->fetchColumn();
        if ($numresult > 0) {
            $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);
        }
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_entidades") {
    $consulta = "SELECT ID,NOMBRE FROM BASE_PARTICIPACIONENTIDAD ORDER BY NOMBRE ASC ";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
       // $numresult = $stmt->fetchColumn();
       // if ($numresult > 0) {
            $RecSet = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);
       // }
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_consulta_doc") {
    $consulta = "SELECT  COUNT(*)  FROM BASE_USUARIO WHERE CEDULA=$documento";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = $stmt->fetchColumn();
     if ($numresult == 0) {

            //$RecSet = $stmt->fetch(PDO::FETCH_OBJ);
                $consulta_1 = "SELECT * FROM RED_SIGEP WHERE DOCUMENTO=$documento AND TIPO_DOCUMENTO_ID=$tipo_doc";
                
                $stmt_1 = $db->prepare($consulta_1);
                $stmt_1->execute();
                $RecSet = $stmt_1->fetch(PDO::FETCH_OBJ);
             
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'data' => $RecSet);
            echo json_encode($datos, JSON_FORCE_OBJECT);

     }else{
            header('Content-Type: application/json');
            $datos = array('code' => 101, 'data' => array());
            echo json_encode($datos);
     }
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_consulta_generacodigo") {
    try {
        $db = $conexion->ConectarBD();
        $codigoverificacion = rand(10, $documento);
        $querydeleteverificacion = "DELETE FROM RED WHERE EMAIL_CONTRATISTA_SERVIDOR = '$correo_verif'";
        $stmt_delete = $db->prepare($querydeleteverificacion);
        $stmt_delete->execute();
        $queryverificacion = "INSERT INTO RED (EMAIL_CONTRATISTA_SERVIDOR, IDENTIDAD_CONTRATISTA_SERVIDOR,CODVERFICACION_CONTRATISTA_SERVIDOR) VALUES('$correo_verif', '$documento', '$codigoverificacion')";
        $stmt_insert = $db->prepare($queryverificacion);
        $valadd = $stmt_insert->execute();


        $db = null;
        $stmt = null;
        header('Content-Type: application/json');
        $datos = array('code' => 100, 'codigoverif' => $codigoverificacion, 'data' => $valadd);
        echo json_encode($datos, JSON_FORCE_OBJECT);

    } catch (PDOException $e) {
        echo ($e->getMessage());
    }

}



if ($VALIDACCESO == "reg_consulta_verif") {
    $consulta = "SELECT *
    FROM  RED red
    LEFT JOIN RED_SIGEP sigep ON sigep.EMAIL_PERSONAL = red.EMAIL_CONTRATISTA_SERVIDOR OR sigep.EMAIL_INSTITUCIONAL = red.EMAIL_CONTRATISTA_SERVIDOR 
    WHERE red.CODVERFICACION_CONTRATISTA_SERVIDOR = $codigo_verif";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = _Count('RED', 'CODVERFICACION_CONTRATISTA_SERVIDOR = '.$codigo_verif);
        if ($numresult > 0) {
                $RecSet = $stmt->fetch(PDO::FETCH_OBJ);
                $db = null;
                $stmt = null;
                header('Content-Type: application/json');
                $datos = array('code' => 100, 'data' => $RecSet);
        } else {
            header('Content-Type: application/json');
            $datos = array('code' => 101, 'data' => array());
           // echo json_encode($datos);
        }
        echo json_encode($datos, JSON_FORCE_OBJECT);

    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
}

if ($VALIDACCESO == "reg_datos_funcionarios") {
    $consulta = "SELECT COUNT(*) FROM BASE_USUARIO WHERE CEDULA=$nidentificacion AND RED_TIPO_IDENTIFICACION=$tidentificacion";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = $stmt->fetchColumn();
     
    if ($numresult == 0) {
        
        
        
        if($tipousuario == 2){
            //$rutaimg = 'C:\laragon\www\funcionPublica\img';
            $rutaimg = '/var/www/html/funcionPublica/img';
            if(isset($foto)&& !empty($foto)){
                $imgfoto=file_get_contents($foto['tmp_name']);
                $tempfoto= explode('.',$foto['name']);
                $extensionfoto = end($tempfoto);
                $namefoto = 'foto_'.$nidentificacion.'.'.$extensionfoto;
                file_put_contents($rutaimg.'/'.$namefoto, $imgfoto);
               
            }
            if(isset($documentov) && !empty($documentov)){
                $imgdocumentov=file_get_contents($documentov['tmp_name']);
                $tempdocumentov= explode('.',$documentov['name']);
                $extensiondocumentov = end($tempdocumentov);
                $namedocumentov = 'documentov_'.$nidentificacion.'.'.$extensiondocumentov;
                file_put_contents($rutaimg.'/'.$namedocumentov, $imgdocumentov);
                
            }
            
            $db->beginTransaction();
            $queryinsert_baseuser = "INSERT INTO 
            BASE_USUARIO (
            LOGIN,
            RED_LOGIN,
            RED_TIPO_IDENTIFICACION,
            CEDULA,
            NOMBRES,
            APELLIDOS,
            CEDULA_EXTRANJERA,
            PASSWORD,
            CELULAR,
            EMAIL,
            ID_PAIS,
            DEPARTAMENTO_RESIDENCIA_ID,
            CIUDAD_RESIDENCIA_ID,
            ENTIDADESIDS,
            ACTIVO,
            \"ONLINE\",
            GRUPO_ID,
            CARGO,
            DIRECTORIO_IMAGEN,
            NOMBRE_IMAGEN,
            DIRECTORIO_DOCUMENTO,
            NOMBRE_DOCUMENTO
            ) VALUES( 
           '$nidentificacion',
           '".$tidentificacion.$nidentificacion."',
           $tidentificacion,
           '$nidentificacion',
           '$nombreCiudadano',
           '$apellidosCiudadano',
           NULL,
           '".sha1($contrasenna)."',
           '$numTelefono',
           '$email_personal',
           $paisResidenciaCiudadano,
           $DepartamentoResidenciaCiudadano,
           $MunicipioResidenciaCiudadano,
           '$entidad',
           1,
           1,
           40,
           '$lst_cargo',
           '$rutaimg',
           '".((isset($namefoto) && !empty($namefoto))?$namefoto:"")."',
           '$rutaimg',
           '".((isset($namedocumentov) && !empty($namedocumentov))?$namedocumentov:"")."'
           )";
            $stmt_insert_base = $db->prepare($queryinsert_baseuser);
            
            $valadd_base = $stmt_insert_base->execute();
            $temasinteresunido = ""; 
               /* if(isset($temasinteres) && !empty($temasinteres)){
                    $temasinteresunido =  implode("|",$temasinteres);
                }*/
                $queryinsert_reduser = "INSERT INTO RED_USUARIOS (
                     \"INT\",
                     FECHA_NACIMIENTO, 
                      TERMINOS_CONDICIONES,
                       TEMA_INTERES_UNO, 
                       TIPO_USUARIO_ID)
                VALUES(
                    (SELECT ID FROM BASE_USUARIO WHERE CEDULA=$nidentificacion AND RED_TIPO_IDENTIFICACION=$tidentificacion),
                    TO_DATE('$Fecha_Nacimiento','yyyy/mm/dd'),
                    ".($terminoscondiciones=="on"?1:0).",
                    '$temasinteresunido',
                    $tipousuario
                )";
                    $stmt_insert_red = $db->prepare($queryinsert_reduser);
                    $valadd_red = $stmt_insert_red->execute();
                    
                    $db->commit();
                  
                    $db = null;
                    $stmt = null;
                    header('Content-Type: application/json');
                    $datos = array('code' => 100, 'val_base' => $valadd_base, 'val_red' => $valadd_red);
        
        }
        if($tipousuario == 1){
            $db->beginTransaction();
            $queryinsert_baseuser = "INSERT INTO 
            BASE_USUARIO (
            LOGIN,
            RED_LOGIN,
            RED_TIPO_IDENTIFICACION,
            CEDULA,
            NOMBRES,
            APELLIDOS,
            CEDULA_EXTRANJERA,
            PASSWORD,
            CELULAR,
            EMAIL,
            ID_PAIS,
            DEPARTAMENTO_RESIDENCIA_ID,
            CIUDAD_RESIDENCIA_ID,
            ACTIVO,
            \"ONLINE\",
            GRUPO_ID
            ) VALUES( 
           '$nidentificacion',
           '".$tidentificacion.$nidentificacion."',
           $tidentificacion,
           '$nidentificacion',
           '$nombreCiudadano',
           '$apellidosCiudadano',
           NULL,
           '".sha1($contrasenna)."',
           '$numTelefono',
           '$email_personal',
           $paisResidenciaCiudadano,
           $DepartamentoResidenciaCiudadano,
           $MunicipioResidenciaCiudadano,
           1,
           1,
           40
           )";
            $stmt_insert_base = $db->prepare($queryinsert_baseuser);
            
            $valadd_base = $stmt_insert_base->execute();
            
                $temasinteresunido = ""; 
                /*if(isset($temasinteres) && !empty($temasinteres)){
                    $temasinteresunido =  implode("|",$temasinteres);
                }*/
                $queryinsert_reduser = "INSERT INTO RED_USUARIOS (
                     \"INT\",
                     FECHA_NACIMIENTO, 
                      TERMINOS_CONDICIONES,
                      INTEGRANTE_JUNTA_ACCION_COMUNAL,
                      PERTENECE_VEEDURIA_CIUDADANA,
                       TIPO_USUARIO_ID)
                VALUES(
                    (SELECT ID FROM BASE_USUARIO WHERE CEDULA=$nidentificacion AND RED_TIPO_IDENTIFICACION=$tidentificacion),
                    TO_DATE('$Fecha_Nacimiento','yyyy/mm/dd'),
                    ".($terminoscondiciones=="on"?1:0).",
                    ".($integrantejunta=="on"?1:0).",
                    ".($pertenceveedura=="on"?1:0).",
                    $tipousuario
                )";
                    $stmt_insert_red = $db->prepare($queryinsert_reduser);
                    $valadd_red = $stmt_insert_red->execute();
                    $db->commit();
                 
                    $db = null;
                    $stmt = null;
                    header('Content-Type: application/json');
                    $datos = array('code' => 100, 'val_base' => $valadd_base, 'val_red' => $valadd_red);
        
        }
        
     }else{
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 101, 'val_base' => $valadd_base, 'val_red' => $valadd_red);
     }
     echo json_encode($datos, JSON_FORCE_OBJECT);
     } catch (PDOException $e) {
        $db->rollBack();
        header('Content-Type: application/json');
        echo json_encode($e->getMessage());
     }
}

if ($VALIDACCESO == "reg_datos_institucional") {
    $consulta = "SELECT COUNT(*) FROM RED_ENLACE_INSTITUCIONAL WHERE NOMBRE_ENTIDAD=$nombre_entidades";
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->prepare($consulta);
        $stmt->execute();
        $numresult = $stmt->fetchColumn();
        if ($numresult == 0) {
            $Id = rand(1, $codsigep);
            $db->beginTransaction();
            $rutaimg = 'D:\XAMPP\htdocs\funcionPublica\img';
            if (isset($foto) && !empty($foto)) {
                $imgfoto = file_get_contents($foto['tmp_name']);
                $tempfoto = explode('.', $foto['name']);
                $extensionfoto = end($tempfoto);
                $namefoto = 'foto_' . $codsigep . '.' . $extensionfoto;
                file_put_contents($rutaimg . '/' . $namefoto, $imgfoto);
            }
            $queryinsert_redinst = "INSERT INTO RED_ENLACE_INSTITUCIONAL (
                    ID, 
                    CODIGO_SIGEP, 
                    NOMBRE_ENTIDAD, 
                    NOMBRE_DEREGIONAL, 
                    TIPO_ENLACE, 
                    DEPARTAMENTO_ENTIDAD_ID, 
                    CIUDAD_ENTIDAD_ID, 
                    EMAIL, 
                    LOGO,
                    PASSWORD,
                    TERMINOS_CONDICIONES,
                    POLITICA_PROTECION) VALUES (
                        '$Id',
                        '$codsigep',
                        '$nombre_entidades',
                        '$tipoenlace',
                        '$nom_regional',
                        '$DepartamentoLaborainstitucional',
                        '$MunicipioLaborainstitucional',
                        '$email_institucional',
                        '$namefoto',
                        '" . sha1($contrasenna) . "',
                        " . ($terminoscondiciones == "on" ? 1 : 0) . ",
                        " . ($politicaproteccion == "on" ? 1 : 0) . "
                        )";

            $stmt_insert_red = $db->prepare($queryinsert_redinst);
            $valadd_red = $stmt_insert_red->execute();
            $db->commit();
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 100, 'val_red' => $valadd_red);
        } else {
            $db = null;
            $stmt = null;
            header('Content-Type: application/json');
            $datos = array('code' => 101, 'val_red' => $valadd_red);
        }
        echo json_encode($datos, JSON_FORCE_OBJECT);
    } catch (PDOException $e) {
        // '" . ((isset($namefoto) && !empty($namefoto)) ? $namefoto : "") . "',
       // $db->rollBack();
       // header('Content-Type: application/json');
        echo ($e->getMessage());
    }
}



function _Count($table, $condicion)
{
    $conexion = new Conexion();
    try {
        $db = $conexion->ConectarBD();
        $stmt = $db->query("SELECT COUNT(*) FROM $table WHERE $condicion");
        $numresult = $stmt->fetchColumn();
    } catch (PDOException $e) {
        echo ($e->getMessage());
    }
    return $numresult;
}

