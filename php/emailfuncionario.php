<?php
error_reporting(0);

extract($_REQUEST);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../PHPMailer/Exception.php';
require '../PHPMailer/PHPMailer.php';
require '../PHPMailer/SMTP.php';

$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = 0; //Enable verbose debug output
    $mail->isSMTP(); //Send using SMTP
    $mail->Host       = 'smtp.office365.com'; //Set the SMTP server to send through
    $mail->SMTPAuth   = true; //Enable SMTP authentication
    $mail->Username   = 'cursosvirtualeseva@funcionpublica.gov.co';                     //SMTP username
    $mail->Password   = '9k3x@76G*TUsJ'; //SMTP password
    $mail->SMTPSecure = 'tls'; //Enable implicit TLS encryption
    $mail->Port       = 25;  //TCP port`

    //Recipients
    $mail->setFrom('cursosvirtualeseva@funcionpublica.gov.co', 'cursos virtuales funcion publica');
    $mail->addAddress($correo, 'Codigo verificacion');     //Add a recipient

    //Content
    $template = file_get_contents('rd-mailform.tpl');
    $mail->isHTML(true);
    //Set email format to HTML
    $Subject = 'Codigo verificacion cursos virtuales de la funcion publica';
    if (isset($correo)) {
        $template = str_replace(
            ["<!-- #{FromState} -->", "<!-- #{FromEmail} -->"],
            ["Email:", $correo],
            $template
        );
    } else {
        die('MF003');
    }

    if (isset($codigo)) {
        $template = str_replace(
            ["<!-- #{MessageState} -->", "<!-- #{MessageDescription} -->"],
            ["CODIGO VERIFICACION PARA SEGUIR CON EL PROCESO DE REGISTRO:", '</br> <b style="font-size: xx-large;">' . $codigo . '</b>'],
            $template
        );
    }

    preg_match("/(<!-- #{BeginInfo} -->)(.|\n)+(<!-- #{EndInfo} -->)/", $template, $tmp, PREG_OFFSET_CAPTURE);
    foreach ($_POST as $key => $value) {
        if ($key != "email" && $key != "message" && $key != "form-type" && !empty($value)
        ) {
            $info = str_replace(
                ["<!-- #{BeginInfo} -->", "<!-- #{InfoState} -->", "<!-- #{InfoDescription} -->"],
                ["", ucfirst($key) . ':', $value],
                $tmp[0][0]
            );

            $template = str_replace("<!-- #{EndInfo} -->", $info, $template);
        }
    }

    $template = str_replace(
        ["<!-- #{Subject} -->", "<!-- #{SiteName} -->"],
        [$subject, $_SERVER['SERVER_NAME']],
        $template
    );

    $mail->CharSet = 'utf-8';
    $mail->Subject = $subject;
    //$mail->MsgHTML($template);

   // $mail->Subject = $Subject;
  //  $mail->Subject = 'Codigo verificacion cursos virtuales de lafuncion publica';
    $mail->Body    =  $template;




    $mail->send();
    echo 'Mensaje Enviado Correctamente';
} catch (Exception $e) {
    echo "Hubo un error al enviar el mensaje: {$mail->ErrorInfo}";
}