$(document).ready(function () {

    // Toolbar extra buttons
    var btnFinish = $('<button></button>').text('Finalizar')
        .addClass('btn btn-finish btn-finish-ml rounded-pill')
        .on('click', function () {
            Funcionario()
            return false;
        });
    
    // Smart Wizard
    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'dots',
        transitionEffect: 'fade',
        toolbarSettings: {
            toolbarPosition: 'bottom',
            toolbarExtraButtons: [btnFinish]
        },
        anchorSettings: {
            markDoneStep: true, // add done css
            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
        }
    });

     $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
         var elmForm = $("#step-" + stepNumber);
         if (stepDirection === 'forward' && elmForm) {
            elmForm.validator('validate');
             var elmErr = $("#step-" + stepNumber + ' .list-unstyled');
            if (elmErr && elmErr.length > 0) {
                return false;
            }
        } 
        return true;
    }); 

    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection) {
        $('.btn-finish').hide();
        if ($("#tiposUsuario").val() == 1 || $("#tiposUsuario").val() == 3) {
            if (stepNumber == 1) {
                $('.btn-finish').show();
                    } else {
                        $('.btn-finish').hide();
                    }
        } else {
            if (stepNumber == 3) {
                $('.btn-finish').show();
            } else {
                $('.btn-finish').hide();
            }
        }
    });

});