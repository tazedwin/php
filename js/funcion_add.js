/* jQuery(function ($) {
    $("#myFormFuncionario").submit( */
function Funcionario() {
    var data = new FormData();
    var arraydata
    if ($("#tiposUsuario").val() == 3) {
        arraydata = $("#myFormInstucional").serializeArray();
    } else {
        arraydata = $("#myFormFuncionario").serializeArray();
    }
   
    for (let item of arraydata) {
        data.append(item.name,item.value)
    }

    if($("#foto").length > 0){
        if($("#foto")[0].files.length > 0){
            data.append('foto',$("#foto")[0].files[0])
        }
    }

    if($("#documentov").length > 0){
        if($("#documentov")[0].files.length > 0){
            data.append('documentov',$("#documentov")[0].files[0])
        }
    }
    
    console.log(data)
    
     $.ajax({
        type: 'POST',
        url: URL + "registrofuncionario.php",
        data: data,//$("#myFormFuncionario").serialize(),
        timeout: 30000,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $("#btnconsulta").html("Enviando Registros");
        }
     }).done(function (json) {
         console.log(json)
        if (json.code == 100) {
            var array = json.data;
            console.log(array)
            var msg = `
      <img src="../img/icono-registroexitoso.svg" alt="icono-error" style="width:15%"></br>
      <span class="msg_sucess_error" style="font-family: 'Signika', sans-serif;font-weight: 500; color: #3366cc;font-size: 2em">¡Bienvenido!</span></br>
      <span class="msg_sucess_error">Ya eres parte de la la Red de Servidores Públicos más grandes de Colombia.</span></br>
      <button class="btn btn-primary" type="button" id="btnmensaje" style="margin-top:20px;width:25%">
                              Ir a la Red
                            </button>`;
            $("#btnconsulta").html("Consultar Identificación");
            $("#selector-msg").show();
            $("#selector-form").hide();
            $("#selector-msg").html(msg);
            $("#btnmensaje").click(function () {
                location.href = "http://localhost/funcionPublica/app";
                /* $("#selector-msg").hide();
                $("#selector-form").show(); */
            });
        }
        return false;
    });
    return false;
}