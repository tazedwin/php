let URL = ruta();
jQuery(function ($) {
  
            //FUNCION QUE CARGA DATOS PARAMETROS
            PARAMETRO_TUSER();
            PARAMETRO_TDOC();
            PARAMETROS();
            PAIS();
          //FIN PROCESO CARGAR PARAMETROS
  

        $("#tiposUsuario").change(function () {
          if ($("#tiposUsuario").val() == 1) {
            $("#app").load('../app/page/ciudadano.html');
            $("#CONTAINER_GENERACION_VERIFICACION").hide();
            $("#CONTAINER_CONSULTAR_IDETIFICACION").show();
            $("#tituloEmail").text("E-Mail Personal");
            

          } else
            if ($("#tiposUsuario").val() == 2) {
              $("#app").load('../app/page/servidor_public_contra.html');
              $("#tituloEmail").text("E-Mail institucional ");
              $("#CONTAINER_VERIFICACION").hide();
              $("#CONTAINER_GENERACION_VERIFICACION").hide();
              $("#CONTAINER_CONSULTAR_IDETIFICACION").show();
            
              
          } else
              if ($("#tiposUsuario").val() == 3) {
                $("#app").load('../app/page/enlace_institucional.html');
                $("#tituloEmail").text("E-Mail Institucional ");
                $("#CONTAINER_GENERACION_VERIFICACION").show();
                $("#CONTAINER_CONSULTAR_IDETIFICACION").hide();
          }
        });
  
        $("#tidentificacion_val").change( () => {$("#error-tipoidet").hide()});
        $("#nidentificacionv").keydown(() => { $("#error-numdoc").hide() });
        $("#emailgralverificacion").keydown(() => { $("#error-email").hide() });
        $("#codverific").keydown(() => { $("#error-codverific").hide() });
        
        $("#btnconsulta").click( () => {
          CONSULTA_DOCUMENTO($("#nidentificacionv").val(), $("#tidentificacion_val").val(), $("#tiposUsuario").val())
        });

        $("#btnverificacion").click(() => {
          CONSULTA_VERIFICACION($("#codverific").val())
        });

        $("#btngralverificacion").click(() => {

          GENERAR_CODIGO($("#nidentificacionv").val(), $("#emailgralverificacion").val())

        });
  $("#CONTAINER_WIZARD").show();
});

function PARAMETRO_TUSER() {

  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_tipouser" }, function (json) {
    if (json.code == 100) {
      var array = json.data;
      op_gruvalor = '';
      for (var i in array) {
        op_gruvalor += "<option value='" + array[i].ID + "'>" + array[i].NOMBRE + "</option>";
      }

       $("#tiposUsuario").append(op_gruvalor);
    }
  });

}

function PARAMETRO_TDOC() {

  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_tipoidentificacion" }, function (json) {
    if (json.code == 100) {
      var array = json.data;
      op_doc = '';
      for (var i in array) {
        op_doc += "<option value='" + array[i].ID + "'>" + array[i].NOMBRE + "</option>";
      }
      $("#tidentificacion_val option").remove("option:gt(0)");
      $("#tidentificacion_val").append(op_doc);
      $("#tidentificacion").append(op_doc);
    }
  });

}

function PARAMETROS() {
  var op_genero = '', op_cargos = '', op_grupet = '', op_equipo = '', op_orden = '', op_enlace='';

  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_parametro" }, function (json) {
    if (json.code == 100) {
      var array = json.data;
      for (var i in array) {

        if (array[i].TIPO == 'GENERO') {
          op_genero += "<option value='" + array[i].CODIGO + "'>" + array[i].VALOR + "</option>";
        }

        if (array[i].TIPO == 'GRUPO_ETNICO') {
          op_grupet += "<option value='" + array[i].CODIGO + "'>" + array[i].VALOR + "</option>";
        }

        if (array[i].TIPO == 'CARGOS') {
          op_cargos += "<option value='" + array[i].CODIGO + "'>" + array[i].VALOR + "</option>";
        }

        if (array[i].TIPO == 'EQUIPO_TRANSVERSAL') {
          op_equipo += "<option value='" + array[i].CODIGO + "'>" + array[i].VALOR + "</option>";
        }

        if (array[i].TIPO == 'ORDEN_ENTIDAD') {
          op_orden += "<option value='" + array[i].CODIGO + "'>" + array[i].VALOR + "</option>";
        }

        if (array[i].TIPO == 'TIPO_ENLACE') {
          op_enlace += "<option value='" + array[i].CODIGO + "'>" + array[i].VALOR + "</option>";
        }

      }

      $("#lst_genero").append(op_genero);
      $("#lst_grupoetnico").append(op_grupet);
      $("#lst_cargo").append(op_cargos);
      $("#etransversal").append(op_equipo);
      $("#ordenentidad").append(op_orden);
      $("#tipoenlace").append(op_enlace);
    }
  });
}

function PAIS() {
  var op_pais = '';
  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_pais" }, function (json) {
    if (json.code == 100) {
      var array = json.data;
      for (var i in array) {
        if (array[i].ESTADO == 1) {
          op_pais += "<option value='" + array[i].ID + "'>" + array[i].NOMBRE + "</option>";
        }
      }
      $("#paisResidenciaCiudadano").append(op_pais);
    }
  });
}

function DPTO(id) {
  var op_dpto = '';
  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_dpto", id: id }, function (json) {
    if (json.code == 100) {
      var array = json.data;
          for (var i in array) {
            op_dpto += "<option value='" + array[i].ID + "'>" + array[i].NOMBRE + "</option>";
          }
      /* LISTA PARA CARGAR EL COMBO DPTO  RESIDENCIA CIUDADANO */
      $("#DepartamentoResidenciaCiudadano option").remove("option:gt(0)");
                    $("#DepartamentoResidenciaCiudadano").append(op_dpto);

      /* LISTA PARA CARGAR EL COMBO DPTO  LABORAL CIUDADANO */
      $("#DepartamentoLaboraCiudadano option").remove("option:gt(0)");
                    $("#DepartamentoLaboraCiudadano").append(op_dpto);

      /* LISTA PARA CARGAR EL COMBO DPTO  LABORAL INSTITUCION */
      $("#DepartamentoLaborainstitucional option").remove("option:gt(0)");
                    $("#DepartamentoLaborainstitucional").append(op_dpto);
      }
    });
}

function CIUDAD(id,sw) {
  var op_ciudad = '';
  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_cuidad", id: id }, function (json) {
    if (json.code == 100) {
      var array = json.data;
      for (var i in array) {
        op_ciudad += "<option value='" + array[i].ID + "'>" + array[i].NOMBRE + "</option>";
      }
      if (sw == 0) {
        $("#MunicipioResidenciaCiudadano option").remove("option:gt(0)");
        $("#MunicipioResidenciaCiudadano").append(op_ciudad);
      } else if (sw == 1){
        $("#MunicipioLaboraCiudadano option").remove("option:gt(0)");
        $("#MunicipioLaboraCiudadano").append(op_ciudad);
      } else if (sw == 2) {
        $("#MunicipioLaborainstitucional option").remove("option:gt(0)");
        $("#MunicipioLaborainstitucional").append(op_ciudad);
      }

    }
  });
}

function ENTIDADES() {
  var op_entidad = '';
  $.getJSON(URL + "registrofuncionario.php", { VALIDACCESO: "reg_entidades" }, function (json) {
    if (json.code == 100) {
      var array = json.data;
      for (var i in array) {
          op_entidad += "<option value='" + array[i].ID + "'>" + array[i].NOMBRE + "</option>";
      }
      $("#nombre_entidades").append(op_entidad);
    }
  });
}

function CONSULTA_DOCUMENTO(CEDULA, TDOC, TUSER) {
  $("#error-tipoidet").hide();
  $("#error-numdoc").hide();

  if (TDOC == null && CEDULA == "") {
    $("#error-tipoidet").html("<b>No selecciono tipo de documento</b>");
    $("#error-tipoidet").show();
    $("#tidentificacion_val").focus();
    $("#error-numdoc").html("<b>No escribio su número de documento</b>");
    $("#error-numdoc").show();
  } else
  if (TDOC == null) {
    $("#error-tipoidet").html("<b>No selecciono tipo de documento</b>");
    $("#error-tipoidet").show();
    $("#tidentificacion_val").focus();
  } else
    if (CEDULA == "") {
      $("#error-numdoc").html("<b>No escribio su número de documento</b>");
      $("#error-numdoc").show();
      $("#nidentificacionv").focus();
    } else {
    var objJSON = {
      VALIDACCESO: "reg_consulta_doc",
      documento: parseInt(CEDULA),
      tipo_doc: TDOC,
      tipo_user: TUSER
    }
   // console.log(objJSON)
  $("#btnconsulta").html("Validando Documento...");
  $.getJSON(URL + "registrofuncionario.php", objJSON, function (json) {
   
    if (json.code == 100) {
      $("#btnconsulta").html("Consultar Identificación");
       if ($("#tiposUsuario").val() == 2) {
        // console.log(json.data)
          $("#email_personal").val(json.data.EMAIL_PERSONAL ? json.data.EMAIL_PERSONAL : $("#emailinst").val())
          $("#tidentificacion").val(json.data.TIPO_USUARIO_ID ? json.data.TIPO_USUARIO_ID : $("#tidentificacion_val").val())
          $("#nidentificacion").val(json.data.DOCUMENTO ? json.data.DOCUMENTO : $("#nidentificacionv").val())
          $("#nombreCiudadano").val(json.data.NOMBRES)
         $("#apellidosCiudadano").val(json.data.APELLIDOS)
         $("#numTelefono").val(json.data.TELEFONO_CONTACTO)
         $("#Fecha_Nacimiento").val(json.data.FECHA_NACIMIENTO)
         $("#lst_grupoetnico").val(json.data.GRUPO_ETNICO_ID)
         $("#lst_genero").val(json.data.GENERO_ID)
         $("#paisResidenciaCiudadano").val(json.data.PAIS_RESIDENCIA_ID)
         DPTO(json.data.PAIS_RESIDENCIA_ID)
         CIUDAD(json.data.DEPARTAMENTO_RESIDENCIA_ID, 0)
         $("#DepartamentoResidenciaCiudadano").prop("disabled",false)
         $("#DepartamentoResidenciaCiudadano").val(json.data.DEPARTAMENTO_RESIDENCIA_ID)
         $("#MunicipioResidenciaCiudadano").prop("disabled", false)
         $("#MunicipioResidenciaCiudadano").val(json.data.MUNICIPIO_RESIDENCIA_ID)
 
         var extension = $("#email_personal").val().substring($("#email_personal").val().lastIndexOf('.') + -3);
         if (isEmail(extension) == false) {
           $("#documento_verif_file").show();
         } else {
           $("#errors-verif_file").hide();
           $("#documento_verif_file").hide();
         }
       }
      
        $("#emailgralverificacion").val(json.data.EMAIL_PERSONAL ? json.data.EMAIL_PERSONAL : json.data.EMAIL_INSTITUCIONAL)
        $("#CONTAINER_GENERACION_VERIFICACION").show();
        $("#CONTAINER_CONSULTAR_IDETIFICACION").hide();
      
      
    }else if (json.code == 101)  {
      var msg = `
      <img src="../img/icono-error.svg" alt="icono-error" style="width:7.5%"></br>
      <span class="msg_sucess_error" style="font-family: 'Signika', sans-serif;font-weight: 500; color: #3366cc;font-size: 2em">¡Atencion!</span></br>
      <span class="msg_sucess_error">El usuario ya se encuentra registrado.</span></br>
      <button class="btn btn-primary" type="button" id="btnmensaje" style="margin-top:20px;width:25%">
                              Regresar
                            </button>`;
      $("#btnconsulta").html("Consultar Identificación");
      $("#selector-msg").show();
      $("#selector-form").hide();
      $("#selector-msg").html(msg);
      $("#btnmensaje").click(function () {
        $("#selector-msg").hide();
        $("#selector-form").show();
      });
    }else {
      var msg = `
      <img src="../img/icono-error.svg" alt="icono-error" style="width:7.5%"></br>
      <span class="msg_sucess_error" style="font-family: 'Signika', sans-serif;font-weight: 500; color: #3366cc;font-size: 2em">¡Error!</span></br>
      <span class="msg_sucess_error">Hubo un error procesando tu solicitud.</span></br>
      <button class="btn btn-primary" type="button" id="btnmensaje" style="margin-top:20px;width:25%">
                              Regresar
                            </button>`;
      $("#btnconsulta").html("Consultar Identificación");
      $("#selector-msg").show();
      $("#selector-form").hide();
      $("#selector-msg").html(msg);
      $("#btnmensaje").click(function () {
        $("#selector-msg").hide();
        $("#selector-form").show();
      });
    }
  });
 }
}

function CONSULTA_VERIFICACION(CODIGO) {
  
  $("#error-codverific").html("");
  $("#error-codverific").hide();
  if (CODIGO == "") {
    $("#error-codverific").html("<b>Escriba codigo generado y enviado a su E-Mail</b>");
    $("#error-codverific").show();
    $("#codverific").focus();
  } else {
    var objJSON = {
      VALIDACCESO: "reg_consulta_verif",
      codigo_verif: parseInt(CODIGO),
    }
    $("#btnverificacion").html("Validando Codigo verificación...");
    $.getJSON(URL + "registrofuncionario.php", objJSON, function (json) {
     // console.log(json)
      if (json.code == 100) {
        $("#btnverificacion").html("Validar código verificación");
        $("#CONTAINER_WIZARD").show();
        $("#CONTAINER_VERIFICACION").hide();

         if ($("#tiposUsuario").val() == 1) {
          $("#email_personal").val(json.data.EMAIL_PERSONAL ? json.data.EMAIL_PERSONAL : $("#emailinst").val())
          $("#tidentificacion").val(json.data.TIPO_USUARIO_ID ? json.data.TIPO_USUARIO_ID : $("#tidentificacion_val").val())
           $("#nidentificacion").val(json.data.DOCUMENTO ? json.data.DOCUMENTO : $("#nidentificacionv").val())
           
          /* $("#nombreCiudadano").val(json.data.NOMBRES)
          $("#apellidosCiudadano").val(json.data.APELLIDOS)
          $("#lst_grupoetnico").val(json.data.GRUPO_ETNICO_ID)
          $("#paisResidenciaCiudadano").val(json.data.PAIS_RESIDENCIA_ID)
          $("#DepartamentoResidenciaCiudadano").val(json.data.DEPARTAMENTO_RESIDENCIA_ID)
          $("#MunicipioResidenciaCiudadano").val(json.data.MUNICIPIO_RESIDENCIA_ID) */

        } 
      } else {
        var msg = `
      <img src="../img/icono-error.svg" alt="icono-error" style="width:7.5%"></br>
      <span class="msg_sucess_error" style="font-family: 'Signika', sans-serif;font-weight: 500; color: #3366cc;font-size: 2em">¡Error!</span></br>
      <span class="msg_sucess_error">Codigo de Verificación Invalido.</span></br>
      <button class="btn btn-primary" type="button" id="btnmensaje" style="margin-top:20px;width:25%">
                              Regresar
                            </button>`;
        $("#btnverificacion").html("Validar código verificación");
        $("#selector-msg").show();
        $("#selector-form").hide();
        $("#selector-msg").html(msg);
        $("#btnmensaje").click(function () {
          $("#selector-msg").hide();
          $("#selector-form").show();
        });
      }
    });
  }
}

function GENERAR_CODIGO(CEDULA, CORREO) {
  var extension = CORREO.substring(CORREO.lastIndexOf('.') + -3);
  $("#error-email").html("");
    $("#error-email").hide();
  if (CORREO=="") {
    $("#error-email").html("<b>No escribio el correo</b>");
    $("#error-email").show();
    $("#emailgralverificacion").focus()
  } else if (validemail(CORREO)==false) {
    $("#error-email").html("<b>Correo no valido</b>");
    $("#error-email").show();
    $("#emailgralverificacion").focus()
  }else if (isEmail(extension)==false && $("#tiposUsuario").val() == 3) {
    $("#error-email").html("<b style='color:#3366cc;'>su correo no es institucional</b>");
    $("#error-email").show();
    $("#emailgralverificacion").focus()
  } else {
    
  var objJSON = {
    VALIDACCESO: "reg_consulta_generacodigo",
    correo_verif: CORREO,
    documento: CEDULA = CEDULA == "" ? '0123456789' : CEDULA,
  }
    $("#btngralverificacion").html("Generando código verificación...");
      $.getJSON(URL + "registrofuncionario.php", objJSON, function (json) {
       // console.log(json)
        if (json.code == 100) {
          SEND_CORREO(CORREO, json.codigoverif);
          $("#btngralverificacion").html("Generar código de verificación");
          $("#CONTAINER_GENERACION_VERIFICACION").hide();
          $("#CONTAINER_VERIFICACION").show();
          $("#emailinst").val(CORREO);
          $("#email_institucional").val(CORREO)
        } else {
          var msg = `
              <img src="../img/icono-error.svg" alt="icono-error" style="width:7.5%"></br>
              <span class="msg_sucess_error" style="font-family: 'Signika', sans-serif;font-weight: 500; color: #3366cc;font-size: 2em">¡Error!</span></br>
              <span class="msg_sucess_error">Hubo un error procesando tu solicitud.</span></br>
              <button class="btn btn-primary" type="button" id="btnmensaje" style="margin-top:20px;width:25%">
                                      Regresar
                                    </button>`;
          $("#btngralverificacion").html("Generar código de verificación");
          $("#selector-msg").show();
          $("#selector-form").hide();
          $("#selector-msg").html(msg);
          $("#btnmensaje").click(function () {
            $("#selector-msg").hide();
            $("#selector-form").show();
          });
        }
      });
 }
}

function SEND_CORREO(correo, codigo) {
  $.get(URL + "emailfuncionario.php", { correo, codigo}, function (json) {
    console.log(json)
  });
}
