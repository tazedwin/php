function isEmail(extension) {
    switch (extension.toLowerCase()) {
        case 'gov.co': case 'edu.co': case 'mil.co':
            return true;
            break;
        default:
            return false;
            break;
    }
}

function validemail(valor) {
    re = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
    if (!re.exec(valor)) {
        return false;
    }
    else return true;
}