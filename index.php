<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta
      name="author"
      content="Mark Otto, Jacob Thornton, and Bootstrap contributors"
    />
    <meta name="generator" content="Hugo 0.88.1" />
    <title>Inicio de sección</title>

    <!-- Bootstrap core CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
      crossorigin="anonymous"
    />

    <!-- Custom styles for this template -->
    <link href="./css/signin.css" rel="stylesheet" />
  </head>
  <body>
    <div class="form-signin">
      <form  action="./app/" method="POST" >
        <h2 class="h3 mb-3 fw-normal align-center">
          Autenticación de usuarios
        </h2>
        <div class="form-floating">
          <input
            type="email"
            class="form-control"
            id="floatingInput"
            placeholder="name@example.com"
          />
          <label for="floatingInput">Usuario</label>
        </div>
        <div class="form-floating">
          <input
            type="password"
            class="form-control"
            id="floatingPassword"
            placeholder="Password"
          />
          <label for="floatingPassword">Contraseña</label>
        </div>

        <button class="w-100 btn btn-lg btn-warning" type="submit">
          Ingresar
        </button>

        <div class="links">
          <a href="crear_usuario.php"><span>Registrarse</span></a> <br />
          <a href="#"><span>Recuperar contraseña</span></a>
        </div>
      </form>
    </div>
  </body>
</html>
